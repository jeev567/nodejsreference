console.log('Starting app.js');
const fs = require('fs');
const os = require('os');
//Notes only executes when some file calls in REQUIRE
const notes = require('./notes');
const _ = require('lodash'); const yargs = require('yargs')

console.log(_.isString('21'));
var filteredArray = _.uniq(['Jeevan', 1, 'Abraham', 1, 2, 3, 4]);
console.log(filteredArray);


var command = process.argv[2];
console.log(process.argv);

if (command === 'add') {
    console.log('Adding new device');
} else if (command === 'list') {
    console.log('Listing all notes');
} else if (command === 'read') {
    console.log('Reading notes');
}
else if (command === 'remove') {
    console.log('Removing all notes');
} else {
    console.log('Commands not recognised ');
}


//YARGS EXAMPLE

const argv = yargs.argv;
console.log('******************************YARG EXAMPLE*********************************');
console.log('Yarg', argv);

if (command === 'add') {

    notes.addNote(argv.title, argv.body);

} else if (command === 'list') {

    notes.getAll();

} else if (command === 'read') {

    notes.read(argv.title);

} else if (command === 'remove') {

    notes.remove(argv.title);

}